package com.qoritek.test.integration.route;

import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RouteTestOK extends CamelSpringTestSupport {

    @Override
    protected AbstractApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(
                "/test-context-ok.xml", RouteTestOK.class);
    }

    @Test
    public void testMainRoute() throws Exception {
        template.sendBody(context.getRoutes().get(0).getEndpoint(), "Hello!!");
    }

}

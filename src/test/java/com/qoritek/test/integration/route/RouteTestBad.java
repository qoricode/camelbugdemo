package com.qoritek.test.integration.route;

import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Will run with Camel 2.17.4 but not 2.18.*
 */
public class RouteTestBad extends CamelSpringTestSupport {

    @Override
    protected AbstractApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(
                "/test-context-bad.xml", RouteTestBad.class);
    }

    @Test
    public void testMainRoute() throws Exception {
        template.sendBody(context.getRoutes().get(0).getEndpoint(), "Hello!!");
    }

}
